#include <iostream>
#include <vector>
#include <string>

template<typename T>

class Stack
{
public:
    void push(T);
    T pop();
    void show();
private:
    std:: vector<T> v;
};

int main() 
{
    Stack<int> a;
    a.push(1);  a.push(2);  a.push(3);
    a.show();
    std::cout << "poped:" << a.pop() << std::endl;
    a.show();

    Stack<std::string> b;
    b.push("Skillbox"); b.push("awesome");
    b.show();
    std::cout << "poped:" << b.pop() << std::endl;
    b.show();

    return 0;
}

template<class T> void Stack<T>::push(T element)
{
    v.push_back(element);
}

template<class T> T Stack<T>::pop()
{
    T element = v.back();
    v.pop_back();
    return element;
}
template<class T> void Stack<T>::show()
{
    std::cout << "stack:";
    for (auto e : v) std::cout << e << " ";
    std::cout << std::endl;
}